
#include <iostream>
#include <string.h>

struct Bag
{
	std::string books[];
};

struct Student
{
	int Age = 12;
	int Height = 160;
	std::string name = "Ivan";

	Bag* myBag = nullptr;

	void GetInfo()
	{
		std::cout << "Student Info:\nName - " << name << "\nAge - " << Age << "\nHeight - " << Height << '\n';
	}
};

int main()
{
	Student* ptr = new Student{ 10, 100, "Paul" };
	ptr->GetInfo();
}


